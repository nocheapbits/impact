# wine collection app

> IMPACT code case

## thoughts and choices

Grundet tidsrammen sat for opgaven er app'en meget simpel.
Appen er bygget som en SPA med Vue.js.
Med tanker om skalering af den, har jeg prøvet at skabe et fundament for at
der kan videreudvikles på den. Scss filer er der lavet en struktur på, her
kan der let tilføjes flere filer efter behov, det kunne være text-styles,
breakpoints, fonts, mixins mm.
Kompenterne er nedbrudt i små spiselige dele for at bevare overskuelighed.
Grundet den simple funktionalitet ligger alle komponenter i samme mappe.
Her kunne der overvejes at bruge "Atomic design methodology" til at strukturere
komponenter efter størrelse i henholdsvis atoms, molecules og organisms mapper,
for også at opnå lettere genanvendelighed af komponenter.

Man kunne overveje om man skulle implementere routing for at få detail-view på
sin egen side, i appen er det lavet som en slags modal.

Form-validering er meget simpel, der kunne godt arbejdes mere med den. Den er
lavet med tanker om sortering og filtrering, skulle jeg nå at lave det.

Der er også gjort lidt tanker om den visuelle præsentation og tilpasning til
forskellige viewport størrelser.

Andre tanker:
UX mæssigt ville det give mening hvis man kunne gå frem/tilbage mellem vine,
når man har åbnet detalje viewet. Var samlingen større ville det måske være rart
med en søgefunktion og en traditionel tabelvisning.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
