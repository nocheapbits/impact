import Vue from 'vue'
import App from './App.vue'

const isProd = process.env.NODE_ENV === 'production'

new Vue({
  el: '#app',
  render: h => h(App)
})
